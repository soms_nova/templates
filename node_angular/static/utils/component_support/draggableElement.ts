﻿angular.module('UtilsModule').directive('draggableElement', function () {
    return {
        restrict: 'A',
        link: function (scope, element) {
            $(element).draggable();
        }
    }
});
