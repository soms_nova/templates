angular.module('UtilsModule').directive('konEllipsis', function ($timeout) {
    return {
        restrict: 'C',
        scope: false,
        link: function ($scope, $element, attr) {
            $element.bind('mouseover', function () {
                if ($element[0].offsetWidth < $element[0].scrollWidth && !$element.attr('title')) {
                    $element.attr('title', $element.text());
                }
            });
        },
        controller: function ($scope, $element) {
        }
    };
});
//# sourceMappingURL=tooltiponoverflow.js.map 
//# sourceMappingURL=tooltiponoverflow.js.map