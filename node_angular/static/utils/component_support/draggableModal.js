angular.module('UtilsModule').directive('modalDraggable', function(){
  return {
    restrict: 'EA',
    link: function(scope, element) {                
      $(element).draggable();            
    }
  }  
});
