angular.module('UtilsModule').directive('konContextArea', function ($timeout) {
    //Look this directive depends on bootstrap-contextmenu.js
    return {
        restrict: 'A',
        scope: false,
        link: function ($scope, $element, attr) {
            $timeout(function () {
                var menuElement = $("#" + attr.konContextArea);
                var options = $scope.$eval(menuElement[0].getAttribute('kon-options'));
                console.log(options);
                options.target = "#" + attr.konContextArea;
                $element.contextmenu(options);
            }, 1);
        },
        controller: function ($scope, $element) {
        }
    };
}); 
//# sourceMappingURL=konContextMenu.js.map