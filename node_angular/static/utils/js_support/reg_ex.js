var UtilsModule;
(function (UtilsModule) {
    var process_jsregex = (function () {
        function process_jsregex() {
        }
        process_jsregex.getRegExObj = function (str, pattern, modifier) {
            if (!process_jsregex.isModiferValid(modifier)) {
                console.error('Modifier not vaild');
                return;
            }
            if (!process_jsregex.isPatternValid(pattern)) {
                console.error('Pattern not vaild');
                return;
            }
            try {
                var re = new RegExp(pattern, modifier);
                var match;
                var i = 1;
                var matchList = [];
                match = re.exec(str);
                while (match != null) {
                    //check for infinite loop
                    if (i > str.length) {
                        console.error('Infinite matches');
                        throw 'Infinite matches';
                    }
                    var matchObj = match;
                    var groupObjList = process_jsregex.getGroupobj(match);
                    matchList.push({
                        match: i,
                        children: groupObjList
                    });
                    i += 1;
                    if (modifier.indexOf('g') === -1) {
                        match = null;
                    }
                    else if (!modifier) {
                        match = null;
                    }
                    else {
                        match = re.exec(str);
                    }
                }
            }
            catch (e) {
                console.error('Regex parsing error');
                matchList = null;
            }
            return matchList;
        };
        process_jsregex.getGroupobj = function (matchObj) {
            if (!matchObj) {
                console.error('match is null');
                return null;
            }
            else if (matchObj.length === 1) {
                console.log('single match , no group');
                var retList = [];
                var groupObj = {
                    start: matchObj.index,
                    end: matchObj.index + matchObj[0].length,
                    value: matchObj[0],
                    group: null
                };
                retList.push(groupObj);
            }
            else if (matchObj.length >= 2) {
                console.log('group match detected');
                var noOfGroups = matchObj.length - 1;
                var startIndex = matchObj.index;
                var retList = [];
                for (var i = 1; i < matchObj.length; i++) {
                    var groupObj = {
                        start: startIndex,
                        end: startIndex + matchObj[i].length,
                        value: matchObj[i],
                        group: i,
                    };
                    retList.push(groupObj);
                    startIndex = startIndex + matchObj[i].length;
                }
            }
            return retList;
        };
        process_jsregex.isPatternValid = function (str) {
            if (str === "") {
                return false;
            }
            return true;
        };
        process_jsregex.isModiferValid = function (modifier) {
            if (typeof modifier !== 'string') {
                console.warn('Not a valid modifier given');
                return false;
            }
            //return false if the given string does have invalid characters , i.e except igm
            var re = /[^igm]/g;
            if (re.test(modifier)) {
                return false;
            }
            return true;
        };
        return process_jsregex;
    }());
    UtilsModule.process_jsregex = process_jsregex;
})(UtilsModule || (UtilsModule = {}));
//# sourceMappingURL=reg_ex.js.map