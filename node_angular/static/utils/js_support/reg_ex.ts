﻿module UtilsModule {
    export interface regExGroup {
        start: number;
        end: number;
        group: number;
        value: string;
    }
    export interface regExMatch {
        match: number;
        children: Array<regExGroup>;
    }

    export class process_jsregex {

        static getRegExObj(str: string, pattern: string, modifier: string): Array<regExMatch> {

            if (!process_jsregex.isModiferValid(modifier)) {
                console.error('Modifier not vaild');
                return;
            }
            if (!process_jsregex.isPatternValid(pattern)) {
                console.error('Pattern not vaild');
                return;
            }
            try {
                var re: RegExp = new RegExp(pattern, modifier);
                var match: RegExpExecArray;
                var i = 1;
                var matchList: Array<regExMatch> = [];

                match = re.exec(str);

                while (match != null) {
                    //check for infinite loop
                    if (i > str.length) {
                        console.error('Infinite matches');
                        throw 'Infinite matches';
                    }
                    var matchObj = match;
                    var groupObjList = process_jsregex.getGroupobj(match);
                    matchList.push({
                        match: i,
                        children: groupObjList
                    });
                    i += 1;

                    if (modifier.indexOf('g') === -1) {
                        match = null;
                    }
                    else if (!modifier) {
                        match = null;
                    }
                    else {
                        match = re.exec(str);
                    }

                }
            }
            catch (e) {
                console.error('Regex parsing error');
                matchList = null;
            }


            return matchList;
        }
        static getGroupobj(matchObj: any): Array<regExGroup> {
            if (!matchObj) {
                console.error('match is null');
                return null;
            }
            else if (matchObj.length === 1) {
                console.log('single match , no group');
                var retList: Array<regExGroup> = [];
                var groupObj: regExGroup = {
                    start: matchObj.index,
                    end: matchObj.index + matchObj[0].length,
                    value: matchObj[0],
                    group: null
                };
                retList.push(groupObj);
            }
            else if (matchObj.length >= 2) {

                console.log('group match detected');

                var noOfGroups = matchObj.length - 1;

                var startIndex = matchObj.index;
                var retList: Array<regExGroup> = [];
                for (var i = 1; i < matchObj.length; i++) {
                    var groupObj: regExGroup = {
                        start: startIndex,
                        end: startIndex + matchObj[i].length,
                        value: matchObj[i],
                        group: i,
                    };

                    retList.push(groupObj);
                    startIndex = startIndex + matchObj[i].length;
                }

            }

            return retList;
        }
        static isPatternValid(str: string): boolean {
            if (str === "") {
                return false;
            }
            return true;
        }
        static isModiferValid(modifier: string): boolean {

            if (typeof modifier !== 'string') {
                console.warn('Not a valid modifier given');
                return false;
            }
            //return false if the given string does have invalid characters , i.e except igm
            var re = /[^igm]/g
            if (re.test(modifier)) {
                return false;
            }
            return true;
        }
    }
}    