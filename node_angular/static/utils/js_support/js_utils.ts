﻿module UtilsModule {
    export class js_util {
        constructor(private $timeout:ng.ITimeoutService) {
        }
        checkParams (paramList) {
            for (var i = 0; i < paramList.length; i++) {
                if (paramList[i] == undefined || paramList[i] == null) {
                    return false;
                }
            }
            return true;
        };
        validateInputs (inputList, checkList) {

            if (!this.checkParams([inputList, checkList])) {
                return false;
            }
            if (inputList.constructor != Array || checkList.constructor != Array) {
                return false;
            }
            if (inputList.length != checkList.length) {
                return false;
            }

            for (var i = 0; i < inputList.length; i++) {
                if (inputList[i] == checkList[i]) {
                    return false;
                }
            }
            return true;

        };

        getName (obj) {
            var funcNameRegex = /function (.{1,})\(/;
            var results = (funcNameRegex).exec((obj).constructor.toString());
            return (results && results.length > 1) ? results[1] : "";
        };
        static genUuid ():string {
            return (+new Date).toString() + Math.floor((Math.random() * 10) + 1)
        };
        print (obj) {
            console.log(JSON.stringify(obj, null, 2));
        };   
        static copyTextToClipboard(text) {
            var textArea: HTMLTextAreaElement = document.createElement("textarea");
            textArea.style.position = 'fixed';
            textArea.style.top = '0';
            textArea.style.left = '0';
            textArea.style.width = '2em';
            textArea.style.height = '2em';
            textArea.style.padding = '0';
            textArea.style.border = 'none';
            textArea.style.outline = 'none';
            textArea.style.boxShadow = 'none';
            textArea.style.background = 'transparent';
            textArea.value = text;
            document.body.appendChild(textArea);
            textArea.select();
            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log('Copying text command was ' + msg);
            } catch (err) {
                console.log('Oops, unable to copy');
            }

            document.body.removeChild(textArea);
        }     
    }
    export class jsonfactory {
        constructor() { }
        getRow(list: Array<any>, property:string, value:any):any {
            if (property.constructor == Array) {
                for (var i = 0; i < list.length; i++) {
                        var row = list[i];
                        for (var j = 0; j < property.length; j++) {
                            if (row[property[j]] != value[j]) {
                                break;
                            }
                            if (j == property.length - 1) {
                                return row;
                            }
                        }
                }
                return null;
            }
            else {
                for (var i = 0; i < list.length; i++) {
                    if (list[i][property] == value) {
                        return list[i];
                    }
                }
                return null;
            }

        };
        getRowIndexByField(list:Array<any>, property:string, value:any):number {
            for (var i = 0; i < list.length; i++) {
                if (list[i][property] == value) {
                    return i;
                }
            }
            return -1;
        };
        insertAtIndex(list, index, item) {
            list.splice(index, 0, item);
        };
        removeAtIndex(list: Array<any>, index:number) {
            list.splice(index, 1);
        };
        setColumnValue(list, columnNames, columnValues, prop, value) {
            for (var i = 0; i < list.length; i++) {
                var row = list[i];
                for (var j = 0; j < columnNames.length; j++) {
                    if (row[columnNames[j]] != columnValues[j]) {
                        break;
                    }
                    if (j == columnNames.length - 1) {
                        row[prop] = value;
                        return;
                    }
                }
            }
        };

        getRows(list: Array<any>, property: string, value: any): Array<any> {
            var retList = [];
            for (var i = 0; i < list.length; i++) {
                if (list[i][property] == value) {
                    retList.push(list[i]);
                }
            }
            return retList;
        }
        swapListElements(list: Array<any>, targetPosition: number, destPosition:number):void {           
            if (targetPosition > destPosition) { 
                var temp = list[destPosition];
                list[destPosition] = list[targetPosition]
                list.splice(targetPosition, 1);
                list.splice(destPosition + 1, 0, temp);

            }
            if (targetPosition < destPosition) {
                var temp = list[destPosition];
                list[destPosition] = list[targetPosition]
                list.splice(targetPosition, 1);
                list.splice(destPosition - 1, 0, temp);
            }            

        };

        getObjFromList(list: Array<any>, keyField: string, valueField: string): Object {
            var retObj = {};
            for (var i = 0; i < list.length; i++) {
                retObj[list[i][keyField]] = list[i][valueField];
            }
            return retObj;

        }

    }    
    export class window {
        static openWindow($mdDialog, template, controller, data): ng.IPromise<{}> {
            data = data || {};
            return $mdDialog.show({
                controller: controller,
                templateUrl: template,
                locals: { data: data },
                clickOutsideToClose: true,
                fullscreen: false
            });
        };
    }

    export class processString {        
        static stringEndsWith(str,suffix) {
            return str.match(suffix + "$") == suffix;
        }
        static countOcurrences(str, value) {
            var regExp = new RegExp(value, "gi");
            return (str.match(regExp) || []).length;
        }
        static removeLastLinefromString(str:string):string {
            var strArray = str.split("\n")
            delete strArray[strArray.length - 1]
            return strArray.join("\n");
        };

        static removeCarriageReturn(str:string):string {
            return str.replace(/\r/g, '')
        }
        static replaceStrAtPosition(str:string,start:number,end:number,strToReplace:string):string {
            return str.substring(0, start) + strToReplace + str.substring(end, str.length);
        }
    }

    export class processDOM {
        //requires JQuery library
        static removeSelectorsInsideElement(element: HTMLElement, selector: string): void {
            var elems = $(element).find('.ui-draggable');
            elems.remove();
        }
        static removeAllElementsWithClassName(className: string):void {
            if (!className) {
                console.error('Give proper inputs , classname');
                return;
            }
            var b = document.getElementsByClassName(className);
            if (!b.length) {
                return;
            }
            while (b[0]) {
                b[0].parentNode.removeChild(b[0]);
            }

        }    
    }

    export class evalWrapper {

        eval() {

        }

        static evalInContext(context:any, evalCode: string) {

        }
    }
}
angular.module('UtilsModule').factory('js_util', ['$timeout', ($timeout) =>new UtilsModule.js_util($timeout)]);
angular.module('UtilsModule').factory('jsonfactory',[()=>new UtilsModule.jsonfactory()]);