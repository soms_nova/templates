//Use this filter to verify an object is empty or not (==={})
//Usage from view ng-hide="obj | isEmpty"
angular.module('UtilsModule')
    .filter('isEmpty', function () {
    var bar;
    return function (obj) {
        for (bar in obj) {
            if (obj.hasOwnProperty(bar)) {
                return false;
            }
        }
        return true;
    };
});
//# sourceMappingURL=angular_filters.js.map