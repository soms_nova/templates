declare module UtilsModule {
    interface regExGroup {
        start: number;
        end: number;
        group: number;
        value: string;
    }
    interface regExMatch {
        match: number;
        children: Array<regExGroup>;
    }
    class process_jsregex {
        static getRegExObj(str: string, pattern: string, modifier: string): Array<regExMatch>;
        static getGroupobj(matchObj: any): Array<regExGroup>;
        static isPatternValid(str: string): boolean;
        static isModiferValid(modifier: string): boolean;
    }
}
