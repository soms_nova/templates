declare module workersModule {
    class logger {
        static log(msg: any): void;
        static info(msg: any): void;
        static warn(msg: any): void;
        static error(msg: any): void;
        static debug(msg: any): void;
    }
    class runValidation {
        globals: any;
        constructor();
        runCode(code: string): void;
        setContext(context: any): void;
        clearContext(): void;
    }
}
