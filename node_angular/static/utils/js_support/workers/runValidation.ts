﻿module workersModule {



    export class logger {
        static log(msg) {
            postMessage({ info: 'logging', data: {msg:msg,logType:'log'}});
        }
        static info(msg) {
            postMessage({ info: 'logging', data: { msg: msg, logType: 'info' } });
        }
        static warn(msg) {
            postMessage({ info: 'logging', data: { msg: msg, logType: 'warn' } });
        }
        static error(msg) {
            postMessage({ info: 'logging', data: { msg: msg, logType: 'error' } });
        }
        static debug(msg) {
            postMessage({ info: 'logging', data: { msg: msg, logType: 'debug' } });
        }
    }
    export class runValidation {
        globals: any = {};
        constructor() {
            
        }
        runCode(code: string) {
            //function callled validate must be present be present in the code snippet.
            var globals = this.globals;
            eval(code);
            var ret = validate();
            postMessage({ info: 'return', data: ret });
            postMessage({ info: 'msg', data: "code  evaluated " });
        }

        setContext(context: any) {
            this.globals = context;
        }
        clearContext() {
            this.globals = {};
        }

    }

    class main {

        static run() {
            var runObj = new runValidation();
            addEventListener('message', function (e) {
                var data = e.data;
                switch (data.cmd) {
                    case 'run':
                        runObj.clearContext();
                        runObj.setContext(data.context);
                        runObj.runCode(data.code);                        
                        break;
                    default:
                        break;
                }
             },false);
            

        }
    }

    main.run();
}