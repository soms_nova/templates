var workersModule;
(function (workersModule) {
    var logger = (function () {
        function logger() {
        }
        logger.log = function (msg) {
            postMessage({ info: 'logging', data: { msg: msg, logType: 'log' } });
        };
        logger.info = function (msg) {
            postMessage({ info: 'logging', data: { msg: msg, logType: 'info' } });
        };
        logger.warn = function (msg) {
            postMessage({ info: 'logging', data: { msg: msg, logType: 'warn' } });
        };
        logger.error = function (msg) {
            postMessage({ info: 'logging', data: { msg: msg, logType: 'error' } });
        };
        logger.debug = function (msg) {
            postMessage({ info: 'logging', data: { msg: msg, logType: 'debug' } });
        };
        return logger;
    }());
    workersModule.logger = logger;
    var runValidation = (function () {
        function runValidation() {
            this.globals = {};
        }
        runValidation.prototype.runCode = function (code) {
            //function callled validate must be present be present in the code snippet.
            var globals = this.globals;
            eval(code);
            var ret = validate();
            postMessage({ info: 'return', data: ret });
            postMessage({ info: 'msg', data: "code  evaluated " });
        };
        runValidation.prototype.setContext = function (context) {
            this.globals = context;
        };
        runValidation.prototype.clearContext = function () {
            this.globals = {};
        };
        return runValidation;
    }());
    workersModule.runValidation = runValidation;
    var main = (function () {
        function main() {
        }
        main.run = function () {
            var runObj = new runValidation();
            addEventListener('message', function (e) {
                var data = e.data;
                switch (data.cmd) {
                    case 'run':
                        runObj.clearContext();
                        runObj.setContext(data.context);
                        runObj.runCode(data.code);
                        break;
                    default:
                        break;
                }
            }, false);
        };
        return main;
    }());
    main.run();
})(workersModule || (workersModule = {}));
//# sourceMappingURL=runValidation.js.map