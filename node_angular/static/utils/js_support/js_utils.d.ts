declare module UtilsModule {
    class js_util {
        private $timeout;
        constructor($timeout: ng.ITimeoutService);
        checkParams(paramList: any): boolean;
        validateInputs(inputList: any, checkList: any): boolean;
        getName(obj: any): string;
        static genUuid(): string;
        print(obj: any): void;
        static copyTextToClipboard(text: any): void;
    }
    class jsonfactory {
        constructor();
        getRow(list: Array<any>, property: string, value: any): any;
        getRowIndexByField(list: Array<any>, property: string, value: any): number;
        insertAtIndex(list: any, index: any, item: any): void;
        removeAtIndex(list: Array<any>, index: number): void;
        setColumnValue(list: any, columnNames: any, columnValues: any, prop: any, value: any): void;
        getRows(list: Array<any>, property: string, value: any): Array<any>;
        swapListElements(list: Array<any>, targetPosition: number, destPosition: number): void;
        getObjFromList(list: Array<any>, keyField: string, valueField: string): Object;
    }
    class window {
        static openWindow($mdDialog: any, template: any, controller: any, data: any): ng.IPromise<{}>;
    }
    class processString {
        static stringEndsWith(str: any, suffix: any): boolean;
        static countOcurrences(str: any, value: any): any;
        static removeLastLinefromString(str: string): string;
        static removeCarriageReturn(str: string): string;
        static replaceStrAtPosition(str: string, start: number, end: number, strToReplace: string): string;
    }
    class processDOM {
        static removeSelectorsInsideElement(element: HTMLElement, selector: string): void;
        static removeAllElementsWithClassName(className: string): void;
    }
    class evalWrapper {
        eval(): void;
        static evalInContext(context: any, evalCode: string): void;
    }
}
