var app = angular.module("testApp");
app.controller("employeeeFormController",["$scope","$mdDialog","data",function($scope,$mdDialog,parameters){
    
    $scope.init = function(){
        $scope.designationList = [{"name":"Developer"},{"name":"Designer"},{"name":"tester"}];
        if(parameters.mode == 'edit'){
            $scope.formTitle = "Edit Employee";
            $scope.employee =  parameters.employee;
            $scope.employeeName = $scope.employee.name;
            $scope.employeeAge = $scope.employee.age;
            $scope.employeeDesignation = $scope.employee.designation;            
        }
        else if(parameters.mode == 'add'){
            $scope.formTitle = "Add Employee";           
            $scope.employeeName = "";
            $scope.employeeAge = "";
            $scope.employeeDesignation = "";    
        }
    }
    $scope.submit = function(isValid) {
        if (isValid) {
            $scope.submitEmployee();           
        }
    };
    $scope.cancel = function() { 
        $scope.cancelDialog(); 
    };        
    
    $scope.submitEmployee = function() {
            if (parameters.mode == 'add') {
                $scope.employee = new Employee($scope.employeeName,$scope.employeeAge,$scope.employeeDesignation);   
                $scope.resolveDialog($scope.employee);
                
            }
            else if (parameters.mode == 'edit') {
                $scope.employee.name =  $scope.employeeName;
                $scope.employee.age =  $scope.employeeAge;
                $scope.employee.designation =  $scope.employeeDesignation;
                $scope.resolveDialog($scope.employee);
            }
    };
    $scope.resolveDialog = function(data) {
       $mdDialog.hide(data);
    
    };
    $scope.cancelDialog = function() {
        $mdDialog.cancel();
    };          
    
    
    $scope.init();
}]);

app.directive('employeeAgeValidation', function () {
    return {
        require: 'ngModel',        
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$validators.validage = function (modelValue, viewValue) {
                if (ctrl.$isEmpty(modelValue)) {
                    return true;
                }
                if(modelValue <= 0 || modelValue > 60){
                    return false;
                }
                return true;
            };
        }
    };
});

