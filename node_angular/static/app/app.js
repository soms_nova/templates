var app = angular.module("testApp",["ngMaterial"]);

app.controller("mainController",["$scope","$mdDialog",function($scope,$mdDialog){
    
    console.log('main controller');
    
    $scope.init = function(){
      $scope.employees = [];  
    };
    $scope.createNewEmployeeClicked = function(){
        var template = "app/forms/employee_input_form.html"
        var controller = "employeeeFormController";
        var data = {"mode":"add"};
        $scope.openWindow(template,controller,data).then(
            function(emp){
                console.log(emp);
                $scope.employees.push(emp);
            },
            function(err){
                console.error("employee create error");
            }
        );
    };
    $scope.updateEmployeeClicked = function(emp,index){
        var template = "app/forms/employee_input_form.html"
        var controller = "employeeeFormController";
        var data = {"mode":"edit","employee":emp};
        $scope.openWindow(template,controller,data).then(
            function(emp){
                console.log(emp);
                //$scope.employees.push(emp);
            },
            function(err){
                console.error("employee create error");
            }
        );
    };
    $scope.deleteEmployeeClicked = function(index){
        $scope.employees.splice(index,1);
    };
    $scope.openWindow = function(template, controller, data) {
            data = data || {};
            return $mdDialog.show({
                controller: controller,
                templateUrl: template,
                locals: { data: data },
                clickOutsideToClose: true,
                fullscreen: false
            });
        };
    
    $scope.init();
    
}]);